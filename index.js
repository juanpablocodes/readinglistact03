// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:
function getAverage(g1, g2, g3, g4, g5) {
	let average = 0;
	average = (g1 + g2 + g3 + g4 + g5) / 5

	if (average <= 74) {
		console.log("Your average is " + Math.round(average) + "." + "Sad to say this but you're in failed mark.")
	} else if (average <= 80) {
		console.log("Your average is " + Math.round(average) + "." + "Keep it up! You are in the Beginner mark!")
	} else if (average <= 85) {
		console.log("Congratulations! Your quarterly average is " + Math.round(average) + "." + "You have received a Developing mark!")
	} else if (average <= 90) {
		console.log("Congratulations! Your quarterly average is " + Math.round(average) + "." + "You have received an Above Average mark!")
	} else if (average <= 100) {
		console.log("Congratulations! Your quarterly average is " + Math.round(average) + "." + "You are currently in Advanced mark!")
	}
}

getAverage(83, 84, 88, 84, 86)

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:
function checkNum(number) {
  	if (number > 300) {
    	return "Invalid Input";
  	}
  	for (x = 1; x <= number; x++) {
    if (x % 2 === 0) {
      	console.log(x + " - even");
    } else {
    	console.log(x + " - odd");
    }
  }
}

checkNum(300);


/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/
const hero = {
    skills:{}
}

hero.heroName = prompt('Hero name:');
hero.origin = prompt('Hero origin:');
hero.description = prompt('Hero description:');
hero.skill1 = prompt('Hero skill 1:');
hero.skill2 = prompt('Hero skill 2:');
hero.skill3 = prompt('Hero skill 3');
console.log(JSON.stringify(hero));
